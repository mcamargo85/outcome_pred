# Outcome-Oriented Predictive Process Monitoring practice
This repository contains the source code necessary to explain the concept of Outcome-Oriented Predictive Process Monitoring. You can find the source code on which this repository is based at https://github.com/irhete/predictive-monitoring-benchmark

## Prerequisites

To execute this code, you need to install Anaconda in your system. Once installed, you can create an environment using the * environment.yml * specification provided in the repository.


## Authors

* ** Manuel Camargo **
* ** Marlon Dumas **
